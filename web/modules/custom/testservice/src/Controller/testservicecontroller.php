<?php
namespace Drupal\testservice\Controller\testservicesontroller;

use \Drupal\Core\Controller\ControllerBase;
use Drupal\testservice;
use Symfony\Component\DependencyInjection\ContainerInterface;

class testservicecontroller extends ControllerBase {
  private $testservice;

  public static function create(ContainerInterface $var) {
    return new static(
      $var->get('testservice.mytestservice')
    );
  }

  public function __construct(testservice\testservice $mytestservice) {
    $this->testservice = $mytestservice;
  }

  public function index() {
    return [
      '#type' => 'markup',
      '#matkup'=> $this->testservice->Test(),
    ];
  }
}
