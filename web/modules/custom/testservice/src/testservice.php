<?php

namespace Drupal\testservice;

use Drupal\Core\Session\AccountInterface;

class testservice {
  protected $currentUser;

  public function __construct(AccountInterface $currentUser)
  {
    $this->currentUser = $currentUser;
  }

  public function getData() {
    return $this->currentUser->getDisplayName();
  }
}
