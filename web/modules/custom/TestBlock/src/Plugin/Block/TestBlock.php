<?php

namespace Drupal\TestBlock\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\path_alias\AliasManager;

/**
 * Provides a 'View Block',
 *
 * @Block (
 * id = "Test Block",
 * admin_label = @Translation ("Test Block"),
 * category = @Translation ("Test Block"),
 * )
 */



class testBlock extends BlockBase {


  public function build()
  {


    return [
      '#markup' => $this->t('Hello Test Block'),
    ];
  }

  public function testblock_preprocess_node(&$variables) {
    $data = \Drupal::service('testservice.mytestservice')->getData;
    var_dump($data);
  }
}

