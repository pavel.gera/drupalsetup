<?php
namespace Drupal\DateBlock\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
 * Show a current date.
 *
 * @Block(
 *   id = "DateBlock",
 *   admin_label = @Translation("Date Block"),
 *   category = @Translation("Date Block"),
 * )
 */
class dateblock extends BlockBase {
  public function build() {
    $date = date("Y M D d");
    return $date;
  }
}
