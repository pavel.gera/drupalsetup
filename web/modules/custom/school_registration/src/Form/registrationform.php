<?php

namespace Drupal\school_registration\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;

class registrationform extends FormBase {

  /**
   * Returns a unique string identifying the form.
   */
public function getFormId() {
  return 'school_registration_form';
}

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array|void
   */
public function buildForm(array $form, FormStateInterface $form_state) {
  $form['student_name'] = [
    '#type' =>  'textfield',
    '#title' => $this->t('Enter your name'),
    '#required' => TRUE,
  ];

  $form['student_tel'] = [
    '#type' => 'tel',
    '#title' => $this->t('Enter your tel №'),
    '#required' => TRUE,
  ];

  $form['accept'] = [
    '#type' => 'checkbox',
    '#title' => $this->t('I accept the terms of use of the site'),
  ];

 $form['actions'] = [
   '#type' => 'actions',
 ];

 $form['actions']['submit'] = [
   '#type' => 'submit',
   '#value' => $this->t('Отправить'),
 ];

 return $form;
}

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return void
   */
public function validateForm(array &$form, FormStateInterface $form_state) {
  if (strlen($form_state->getValue('student_name')) < 3) {
    $form_state->setErrorByName('student_name', $this->t('Please enter a valid name'));
  }
  if (strlen($form_state->getValue('student_tel')) < 5) {
    $form_state->setErrorByName('student_tel', $this->t('Please input a correct tel №'));
  }
}

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return void
   */
public function submitForm(array &$form, FormStateInterface $form_state)
{
  $messenger= \Drupal::messenger();
  $messenger->addMessage('Name ' . $form_state->getValue('student_name'));
  $messenger->addMessage('Phone ' . $form_state->getValue('student_tel'));

  // Redirect to home page.
  $form_state->setRedirect('<front>');
}
}
