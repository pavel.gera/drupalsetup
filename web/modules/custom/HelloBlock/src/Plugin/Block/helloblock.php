<?php

namespace Drupal\helloblock\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
 * This Block show a text "Hello, Block!'
 *
 * @Block (
 *  id = "HelloBlock",
 *  admin_label = @Translation ("HelloBlock"),
 *  category = @Translation ("HelloBlock")
 * )
 */

class helloblock extends BlockBase  {
  /**
   * @inheritDoc
   */
  public function build()
  {
    $renderable = [
      '#markup' => $this->t('Hello, Block!'),
      '#theme' => 'helloblock_template',
      '#test_var' => 'Hello, template!',
    ];
    /**
     * Logging
     */
    \Drupal::logger('helloblock')->notice('Show Block once');
    return $renderable;

  }
}

