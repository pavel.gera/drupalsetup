<?php

namespace Drupal\BasicSiteInfo\Plugin\Block;
use Drupal\Core\Block\BlockBase;


/**
 * Show a basic site info.
 *
 * @Block (
 *   id = "BasicSiteInfo",
 *   admin_label = @Translation ("BasicSiteInfo"),
 *   category = @Translation ("BasicSiteInfo")
 * )
 */

class basicsiteinfo extends BlockBase
{
  public function build()
  {
    $renderable = [
      '#theme' => 'basicsiteinfo_template',
      '#name',
      '#slogan',
    ];
    return $renderable;
  }
}
