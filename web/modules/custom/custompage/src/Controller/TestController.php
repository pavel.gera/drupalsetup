<?php

namespace drupal\custompage\Controller;

use Drupal\Core\Logger\RfcLoggerTrait;
use Psr\Log\LoggerInterface;

/**
 * TestController open new empty test page.
 */
class TestController implements LoggerInterface {
  use RfcLoggerTrait;
  public function ViewPage () {
    return [
      '#markup' => 'Hello, world! This is new custom page',
    ];
  }

  /**
   * Logging everytime variable is called.
   */
  public function log($level, $message, array $context = array())
  {
    echo "This is log of custom page";
  }
}
