<?php
namespace Drupal\schoolservice\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\schoolservice\myschoolservice;
use Symfony\Component\DependencyInjection\ContainerInterface;

class schoolservicesontroller extends ControllerBase {
  protected $my_service;


  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('schoolservice.myschoolservice')
    );
  }
  public function __construct(myschoolservice $my_service) {
    $this->my_service=$my_service;
  }

  public function index() {
    return [
      '#type' => 'markup',
      '#markup' => $this->my_service->myLogic()
      ];
  }


}

$service = \Drupal::service('current_user');

