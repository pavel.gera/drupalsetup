<?php

/**
 * Configuration config_test form.
 */
namespace Drupal\config_test\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ConfigTestForm extends ConfigFormBase {

  /**
   * @return string
   */
  public function getFormId() {
    return 'config_test_admin_settings';
  }

  public function getEditableConfigNames()
  {
    return [
      'school.config_test',
    ];
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config('school.config_test');
    $form['New Form'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter your data'),
      '#default_value' => $config->get('some_text'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return void
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $this->config('school.config_test')
      ->set('some_text', $form_state->getValue('Test text'))
      ->save();
    parent::submitForm($form, $form_state);
  }
}

