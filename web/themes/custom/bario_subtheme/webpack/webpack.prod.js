const { merge } = require('webpack-merge');
const TerserPlugin = require('terser-webpack-plugin');
const webpackConfig = require('./webpack.common');

module.exports = [
  merge(webpackConfig, {
    mode: "production",
    optimization: {
      minimizer: [
        new TerserPlugin({
          cache: false,
          parallel: true,
        }),
      ],
    },
  }),
];
